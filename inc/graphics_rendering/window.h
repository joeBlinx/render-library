#pragma once
#include <string>
#include <SDL2/SDL.h>
namespace render
{
    struct window_settings{
        std::string title;
        float width, height;
    };
    struct window{
        window(window_settings const& settings);
        window_settings const& get_settings() const;
        void refresh();
        void clean();
        ~window();

    private:
        window_settings const& settings;
        SDL_Window* sdl_window;
        SDL_GLContext context;

    };
} // namespace render
