#pragma once

#include <glish3/programGL.hpp>
#include <glish3/Vao.hpp>
#include <string_view>
#include <span>
#include <glish3/Vbo.hpp>
struct RenderingPipeline{
    RenderingPipeline(std::span<float> vertices, glish3::vbo_settings const& vbo_settings, std::string_view vertex, std::string_view fragment);

    glish3::Uniform &operator[](const std::string &name);
private:
    glish3::Vao vao;
    glish3::ProgramGL programgl;
};