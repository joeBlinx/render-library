
#include <glish3/Vbo.hpp>
#include <glish3/shader.hpp>
#include <string_view>
#include <algorithm>
#include <iterator>
#include "shader.h"
#include "render_function.h"
#include "renderer.h"
#include "camera.h"
using namespace std::string_literals;
namespace render{
template<class ...Ts>
renderer<Ts...>::renderer(std::unordered_map<std::string, RenderingPipeline>& rendering, Camera const& camera):
                rendering_pipelines(rendering),
                camera(&camera){
    
    float vertices[] = {
        0, 0,
        1, 0,
        0, 1,
        1, 1
    };
    using namespace render::shader;
    if constexpr (type_trait::has_type<render_data::color, Ts...>()){
        rendering_pipelines.try_emplace("color"s, RenderingPipeline{std::span<float>(vertices), glish3::vbo_settings(2), get_vertex_color(), get_fragment_color()});
    }
    if constexpr (type_trait::has_type<render_data::texture, Ts...>()){
        rendering_pipelines.try_emplace("texture"s, RenderingPipeline{std::span<float>(vertices), glish3::vbo_settings(2), get_vertex_texture(), get_fragment_texture()});
    }
    if constexpr (type_trait::has_type<render_data::sprite, Ts...>()){
        rendering_pipelines.try_emplace("sprite"s, RenderingPipeline{std::span<float>(vertices), glish3::vbo_settings(2), get_vertex_sprite(), get_fragment_sprite()});
    }
}
template<class ...Ts>
void renderer<Ts...>::remove(renderer<Ts...>::const_iterator it){
    order.remove(std::find_if(begin(order), end(order), [&it](auto const& layer){
        return layer.it = it;
    }));
    render_datas.erase(it);
}

template<class ...Ts>
void renderer<Ts...>::render() {
    std::for_each(order.begin(), order.end(), 
    [this](auto const& layer){
        std::visit([this](auto const& render_data){
            render_function::render(*this, render_data);
        }, render_datas[layer.it]);
        
    });
  
}
template<class ...Ts>
template<class T> 
typename renderer<Ts...>::const_iterator renderer<Ts...>::add(T && data){
    auto it = render_datas.emplace_back(Data(std::forward<T>(data)));
    order.insert(layer{data.layer, it});
    return it;
}
template<class ...Ts>
RenderingPipeline& renderer<Ts...>::get_rendering_pipeline(std::string const& key){
    return rendering_pipelines.at(key);
}

template<class... Ts>
template<class T>
T& renderer<Ts...>::get_data(renderer::const_iterator i) {
    auto& variant = render_datas[i];
    assert(std::holds_alternative<T>(variant));
    return std::get<T>(variant);
}
template<class ...Ts>
std::vector<typename renderer<Ts...>::const_iterator> renderer<Ts...>::add_tilemap(render::tile_map const& tile_map){
    static_assert(type_trait::has_type<render_data::sprite, Ts...>(), "You're renderer must hold the type render_data::sprite to be able to add tilemap");
    std::vector<int> ids;
    std::for_each(std::begin(tile_map), std::end(tile_map), [this, &ids](auto& sprite) {
        ids.push_back(add(sprite));
    });
    return ids;
}
template<class ...Ts>
void renderer<Ts...>::remove(std::vector<typename renderer<Ts...>::const_iterator> && ids){
    std::for_each(begin(ids), end(ids), [this](auto id){
        remove(id);
    });
    ids.clear();
}

template<class ...Ts>
std::array<float, 9> renderer<Ts...>::get_matrix_view()const{
    return camera->get_matrix_view();
}


}

