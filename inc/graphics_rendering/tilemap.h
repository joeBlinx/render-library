//
// Created by stiven on 08/11/2019.
//

#ifndef GRAPHICS_RENDERING_TILEMAP_H
#define GRAPHICS_RENDERING_TILEMAP_H

#include "render_data.h"
#include <glish3/texture2d.hpp>
#include <string_view>
namespace render{
    struct tile_map{
        using const_iterator = std::vector<render_data::sprite>::const_iterator;
        tile_map(std::string_view path_to_map);
        const_iterator begin() const {return std::begin(sprites);}
        const_iterator end() const {return std::end(sprites);}
    private:
        std::vector<render_data::sprite> sprites;
        glish3::Texture2D texture;
        sprite_sheet_data sprite_sheet;
        struct tile_param{
            int nb_tile_height{};
            int nb_tile_width{};
            int tile_height{};
            int tile_width{};
        };
        struct pos{
            int x{}, y{};
        } ;
        void handle_layer(nlohmann::json const& layer, pos const& position, tile_param const& param);
    };
}
#endif //GRAPHICS_RENDERING_TILEMAP_H
