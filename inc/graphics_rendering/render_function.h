#pragma once
#include "render_data.h"
#include <array>
namespace render::render_function{
std::array<float, 9> get_transformation_matrix(render::transform const& transform){
    return {
            static_cast<float>(transform.width), 0, 0,
            0,static_cast<float>(transform.height), 0,
            static_cast<float>(transform.x), static_cast<float>(transform.y), 1
    };
}
#ifndef NO_SPRITE_IMPLEMENTATION
template<class ...Ts>
void render(renderer<Ts...>& renderer, render_data::sprite const& datas) {

    auto& transform = datas.data_texture.data_transform;
    auto transformation_model = get_transformation_matrix(transform);
    int texture_id = 0;
    datas.data_texture.texture.activeTexture(texture_id);
    auto& rendering_pipeline = renderer.get_rendering_pipeline("sprite");
    rendering_pipeline["model"] = transformation_model.data();
    rendering_pipeline["id_texture"] = &texture_id;
    rendering_pipeline["view"] = renderer.get_matrix_view().data();

    auto& sprite_data = datas.sprite_sheet_datas->sprite_datas[datas.sprite_number];
    rendering_pipeline["uv_origin"] = &sprite_data;
    rendering_pipeline["uv_size"] = &sprite_data.width;

    glishDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
}
#endif
#ifndef NO_TEXTURE_IMPLEMENTATION
template<class ...Ts>
void render(renderer<Ts...>& renderer, render_data::texture const& datas) {

    auto& transform = datas.data_transform;
    auto transformation_model = get_transformation_matrix(transform);
    int texture_id = 0;
    datas.texture.activeTexture(texture_id);
    auto& rendering_pipeline = renderer.get_rendering_pipeline("texture");
    rendering_pipeline["model"] = transformation_model.data();
    rendering_pipeline["id_texture"] = &texture_id;
    glishDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
}
#endif
#ifndef NO_COLOR_IMPLEMENTATION
template<class ...Ts>
void render(renderer<Ts...>& renderer,render_data::color const& datas){

    auto& transform = datas.data_transform;
    auto transformation_model = get_transformation_matrix(transform);
    color const& color_to_send = datas.data_color;
    float color[] = {
        color_to_send.r.to_unit(),
        color_to_send.g.to_unit(),
        color_to_send.b.to_unit(),
        color_to_send.a.to_unit()
    };
    auto& rendering_pipeline = renderer.get_rendering_pipeline("color");
    rendering_pipeline["model"] = transformation_model.data();
    rendering_pipeline["color"] = color;
    glishDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
}
#endif
}
