#pragma once
#include <glish3/texture2d.hpp>
#include <string_view>
#include <vector>
#include "json/json.hpp"
namespace render{
    struct color_type{
        color_type(int value_color){
            if (value_color < 0 )
                value = 0;
            else if(value_color > 255)
                value = 255;
            else
                value = value_color;
        }
        operator int() {return value;}
        float to_unit() const{
            return value/255.0f;
        }
        color_type():value(0){}
    private:
        int value;
    };
    struct color{
        color_type r, g, b, a{255};
    };
    struct transform{
        int x, y;
        int width, height;
    };
    struct sprite_sheet_data{
        struct sprite_data{
            float u, v, width, height;
        };
        std::vector<sprite_data> sprite_datas;
        sprite_sheet_data(std::string_view path_to_data);
        sprite_sheet_data(nlohmann::json const& tilesets);
        sprite_sheet_data() = default;

    };

    namespace render_data {
        struct layer{ int layer{};};
        struct color :layer{
            transform data_transform{};
            render::color data_color;
        };
        struct texture :layer {
            transform data_transform{};
            glish3::view::texture2d_view texture;
        };

        struct sprite :layer{
            render_data::texture data_texture;
            int sprite_number{};
            sprite_sheet_data* sprite_sheet_datas = nullptr;

        };
    }

}