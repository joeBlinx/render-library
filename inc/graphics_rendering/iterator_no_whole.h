#pragma once
#include <vector>
namespace render{
enum class State{
    NONE,
    SOME
};
template<class T>
struct Schrodinger{
    T value;
    State state;
    Schrodinger(T const& value, State state):value(value), state(state){}
    Schrodinger(T &&value, State state):value(std::move(value)), state(state){}
};
template<class ...>
struct iterator_def;

template<class T, class IT>
struct iterator_def<std::vector<Schrodinger<T>>, IT>{
    using type_container = std::vector<Schrodinger<T>>;
    using iterator = IT;
    T& operator*(){
        return it->value;
    }
    T& operator->(){
        return it->value;
    }
    iterator_def operator++(){
        return iterator_def(find_next_iterator(), container);
    }
    iterator_def operator++(int){
        auto temp = *this;
        (void)find_next_iterator();
        return temp;
    }
    bool operator!=(iterator_def const& other_it){
        return it != other_it.it;
    }
    bool operator==(iterator_def const& other_it){
        return it == other_it.it;
    }
    iterator_def(iterator it, type_container const& container):it(it), container(container){}
private:
    type_container const& container;
    iterator it;
    iterator find_next_iterator(){
        auto end = std::end(container);
        do{
            it++;
        }while(it != end && it->state != State::SOME);
        return it;
    }

};
template<class T>
using iterator = iterator_def<std::vector<Schrodinger<T>>, typename std::vector<Schrodinger<T>>::iterator>;
template<class T>
using const_iterator = iterator_def<std::vector<Schrodinger<T>>, typename std::vector<Schrodinger<T>>::const_iterator>;
}