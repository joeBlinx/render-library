#pragma once

#include <vector>
#include <stack>
#include "iterator_no_whole.h"
namespace render{
template<class T>
struct container_no_whole
{

    using type = Schrodinger<T>;
    using reference = type&;
    using container = std::vector<type>;
    using iterator = iterator<T>;
    using const_iterator = const_iterator<T>;

    iterator begin() {return {std::begin(values)+first_index, values};}
    iterator end() {return {std::end(values), values};}
    const_iterator begin() const{return {std::begin(values), values};}
    const_iterator end() const{return {std::end(values), values};}

    T& operator[](int i){return values[i].value;}
    T const& operator[](int i)const{return values[i].value;}
    template<class U>
    int emplace_back(U&& value);

    void erase(int index);

private:
	int first_index = 0;
    container values;
    std::stack<int> wholes;
	void find_first_index();
};

template<class T>
template<class U>
int container_no_whole<T>::emplace_back(U&& value){
    if (!wholes.empty()){
        int index = wholes.top();
        wholes.pop();
        values[index] = Schrodinger{std::forward<T>(value), State::SOME};
		if(index < first_index){
			first_index = index;
		}
        return index;
    }
    values.emplace_back(Schrodinger{std::forward<T>(value), State::SOME});
    return static_cast<int>(values.size() - 1);
}
template<class T>
void container_no_whole<T>::erase(int index){
    assert(values[index].state != State::NONE);
    values[index].state = State::NONE;
	wholes.push(index);
	find_first_index();
}
template<class T>
void container_no_whole<T>::find_first_index(){
	while (first_index < values.size() 
			&& values[first_index++].state != State::SOME);
	
}
}