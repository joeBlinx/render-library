#pragma once
namespace render::type_trait{

template<class T, class ...Ts>
constexpr bool has_type(){
    return (std::is_same_v<T, Ts> || ...);
}

}