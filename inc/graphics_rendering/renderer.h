#pragma once

#include <glish3/Vao.hpp>
#include <glish3/programGL.hpp>
#include "rendering_pipeline.h"

#include <variant>
#include "container_no_whole.h"
#include <array>
#include "graphics_rendering/tilemap.h"
#include <unordered_map>
#include <graphics_rendering/utils/type_trait.h>
#include <set>

namespace render{
    struct Camera;
    template<class ...Ts>
    struct renderer{
        using Data = std::variant<Ts...>;
        using const_iterator = int;
        renderer(std::unordered_map<std::string, RenderingPipeline>& rendering, Camera const& camera);

        template<class T> 
        [[nodiscard]] const_iterator add(T && data);

        std::array<float, 9> get_matrix_view()const;
        void remove(renderer::const_iterator it);
        void remove(std::vector<const_iterator> && ids);
        void render();
        [[nodiscard]] std::vector<const_iterator> add_tilemap(render::tile_map const& tile_map);
        RenderingPipeline& get_rendering_pipeline(std::string const& key);
        template<class T>
        T& get_data(const_iterator i);
    private:
        Camera const* camera = nullptr;
        container_no_whole<Data> render_datas;
        struct layer{
            int layer_number;
            const_iterator it;
            bool operator<(layer const& l)const{
                return layer_number < l.layer_number;
            }
        };
        std::multiset<layer> order;
        std::unordered_map<std::string, RenderingPipeline>& rendering_pipelines;
        
    };


}
#include "renderer.tpp"

