#pragma once
#include <array>
namespace render{
struct Camera{
    Camera(float x, float y, float width, float height);
    std::array<float, 9> get_matrix_view() const;
    void move(float x, float y);
private:
    float x,y;   
    float width, height;
};
}