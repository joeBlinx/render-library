#pragma once
#include <string_view>
namespace render::shader{
constexpr std::string_view get_vertex_color(){
    return "\
    #version 330 core\n\
    layout (location = 0) in vec2 pos;\n\
    void main(){\n\
    gl_Position = vec4(view*model*vec3(pos, 1), 1);\n\
    }\
    ";
}

constexpr std::string_view get_fragment_color(){
    return "\
    #version 330 core\n\
    out vec4 color_output;\n\
    uniform vec4 color;\n\
    void main(){\n\
    color_output =  color;\n\
    }\
    ";
}

constexpr std::string_view get_vertex_texture(){
    return "\
    #version 330 core\n\
    layout (location = 0) in vec2 pos;\n\
    out vec2 uv_out;\n\
    void main(){\n\
    gl_Position = vec4(view*model*vec3(pos, 1), 1);\n\
    uv_out = pos;\n\
    }\
    ";
}

constexpr std::string_view get_fragment_texture(){
    return "\
    #version 330 core\n\
    out vec4 color_output;\n\
    in vec2 uv_out;\n\
    uniform sampler2D id_texture;\n\
    void main(){\n\
    color_output =  texture(id_texture, uv_out);\n\
    }\
    ";
}

constexpr std::string_view get_vertex_sprite(){
    return "\
    #version 330 core\n\
    uniform vec2 uv_origin;\n\
    uniform vec2 uv_size;\n\
    layout (location = 0) in vec2 pos;\n\
    out vec2 uv_out;\n\
    void main(){\n\
    gl_Position = vec4(view*model*vec3(pos, 1), 1);\n\
    uv_out = mix(uv_origin, uv_origin+uv_size, pos);\n\
    }\
    ";
}
constexpr std::string_view get_fragment_sprite(){
    return get_fragment_texture();
}

}