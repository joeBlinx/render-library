#include <graphics_rendering/window.h>
#include <graphics_rendering/renderer.h>
#include <vector>
#include <variant>
#include <iostream>
#include <glish3/log/errorHandler.hpp>
#include <graphics_rendering/tilemap.h>
#include <unordered_map>

int main(){
    using namespace render;
    render::window_settings settings{"test", 1378, 768};
    render::window window{settings};
    glish3::init("log/", "test project");
    Camera camera{0.f, 0.f, settings.width, settings.height};
    bool run = true;
    SDL_Event ev;
    std::unordered_map<std::string, RenderingPipeline> renderings_pipeline;

    render::renderer<render_data::sprite, render_data::color> renderer(
        renderings_pipeline, camera);

    render::tile_map tile_map{"tilemap/real_map.json"};
    auto ids = renderer.add_tilemap(tile_map);
    while(run){
        window.clean();
        while(SDL_PollEvent(&ev)){
            switch (ev.type){
                case SDL_QUIT:
                    run = false;
                    break;
                case SDL_KEYDOWN:
                    switch(ev.key.keysym.sym){
                        case SDLK_LEFT:
                            camera.move(10.f, 0);
                            break;
                        case SDLK_RIGHT:
                            camera.move(-10.f, 0);
                            break;
                    }
            }
        }
        renderer.render();
        SDL_Delay(17);
        window.refresh();
    }
}