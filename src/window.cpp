#include "window.h"
#include <glish3/gl_glew.hpp>
#include <glish3/glfunction.hpp>
#include <iostream>
namespace render
{
window::window(window_settings const &settings) : settings(settings)
{
    if (SDL_Init(SDL_INIT_EVERYTHING))
    {
        throw std::runtime_error("error while initialize SDL2 " + std::string{SDL_GetError()});
    }
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);

    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
    
    sdl_window = SDL_CreateWindow(settings.title.c_str(), SDL_WINDOWPOS_CENTERED,SDL_WINDOWPOS_CENTERED, settings.width,
                                settings.height, SDL_WINDOW_SHOWN | SDL_WINDOW_OPENGL);
    if (!sdl_window)
    {
        std::string erreur{SDL_GetError()};
        SDL_Quit();
        throw std::runtime_error("error while initialize window " + erreur);
    }
    context = SDL_GL_CreateContext(sdl_window);
    glewExperimental = GL_TRUE;
    auto err = glewInit();
    if (err != GLEW_OK)
    {
        std::cerr << glewGetErrorString(err) << std::endl;
        throw std::runtime_error("error while initialize glew");
    }
    glGetError();
    glClearColor(0.5, 0.5, 0.5, 1);
    glGetError();
    glEnable(GL_BLEND);
    glGetError();
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glGetError();
}
window::~window()
{
    SDL_GL_DeleteContext(context);
    SDL_DestroyWindow(sdl_window);
    SDL_Quit();
}
window_settings const& window::get_settings() const{
    return settings;
}
void window::refresh()
{
    SDL_GL_SwapWindow(sdl_window);
}
void window::clean(){
    glishClear(GL_COLOR_BUFFER_BIT);
}
} // namespace render