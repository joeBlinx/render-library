//
// Created by stiven on 05/11/2019.
//
#include "render_data.h"
#include "json/json.hpp"
#include <fstream>

namespace render {
static float divide(int quotient, int divider) {
    return static_cast<float>(quotient) /
           static_cast<float>(divider);
}

sprite_sheet_data::sprite_sheet_data(std::string_view path_to_data) {
    using json = nlohmann::json;
    json j;
    std::ifstream(path_to_data) >> j;
    struct {
        int width, heigth;
    } texture{};
    auto &texture_json = j["texture"];
    texture.width = texture_json["width"];
    texture.heigth = texture_json["height"];
    auto &sprites_json = j["sprites"];
    for (auto &sprite : sprites_json) {
        sprite_datas.emplace_back(
                sprite_data{
                        .u = divide(sprite["u"], texture.width),
                        .v = divide(sprite["v"], texture.heigth),
                        .width = divide(sprite["width"], texture.width),
                        .height = divide(sprite["height"], texture.heigth)
                });
    }
}

sprite_sheet_data::sprite_sheet_data(nlohmann::json const &tilesets) {
    int tile_count = tilesets["tilecount"];
    int tile_width = tilesets["tilewidth"];
    int tile_height = tilesets["tileheight"];
    int image_height = tilesets["imageheight"];
    int image_width = tilesets["imagewidth"];
    int columns = tilesets["columns"];

    float tile_height_clamp_0_1 = divide(tile_height, image_height);
    float tile_width_clamp_0_1 = divide(tile_width, image_width);
    sprite_datas.resize(tile_count);
    int count{};
    float u{}, v{};
    float increment_u = divide(1, columns);
    float increment_v = divide(1, tile_count / columns);
    for (auto &sprite : sprite_datas) {
        sprite.u = u;
        sprite.v = v;
        sprite.width = tile_width_clamp_0_1;
        sprite.height = tile_height_clamp_0_1;
        if(count == columns-1){
            count = 0;
            u = 0;
            v+= increment_v;
        }else{
            count++;
            u += increment_u;
        }
    }

}
}
