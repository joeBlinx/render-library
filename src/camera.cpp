#include "graphics_rendering/camera.h"
namespace render{

Camera::Camera(float x, float y, float width, float height):x(x), y(y), width(width), height(height){}

std::array<float, 9> Camera::get_matrix_view() const{
    float trans_x = (2.0f/width) * x - 1;
    float trans_y =  y*(-2.0f/height) + 1;

    return { 2.0f/width, 0.f, 0.f,
            0.f, -2.0f/height, 0.f,
            trans_x, trans_y, 1.f};
}
// in comment only for remember the matrix
// std::array<float, 9> Camera::get_transformation_view()const{
//         return {
//             2.0f/width, 0, 0, //1st row
//             0, -2.0f/height, 0, //2nd row
//             -1, 1, 1 //3rd row
//         };
// }
void Camera::move(float delta_x, float delta_y){
    x += delta_x;
    y += delta_y;
}
}