#include "rendering_pipeline.h"
#include <algorithm>
using namespace std::string_literals;
static std::string add_vertex_built_in_variable(std::string_view vertex_view){
    constexpr std::string_view built_in_variable = 
    "uniform mat3 model;\n\
    uniform mat3 view;\n";
    std::string vertex {vertex_view};
    auto it_sharp = std::find(begin(vertex), end(vertex), '#');
    auto second_line = std::find(it_sharp, end(vertex), '\n') + 1;
    vertex.insert(second_line, begin(built_in_variable), end(built_in_variable));
    return vertex;
}
RenderingPipeline::RenderingPipeline(std::span<float> vertices,  glish3::vbo_settings const& vbo_settings, std::string_view vertex_view, std::string_view fragment_view){

    using glish3::Shader;
    vao.addVbo(glish3::Vbo (GL_ARRAY_BUFFER, vertices, vbo_settings));
    std::string vertex = add_vertex_built_in_variable(vertex_view);
    Shader vertex_shader = Shader::createShaderFromData(GL_VERTEX_SHADER, vertex);
    Shader fragment_shader = Shader::createShaderFromData(GL_FRAGMENT_SHADER, fragment_view);
    programgl = glish3::ProgramGL(vertex_shader, fragment_shader);
}

glish3::Uniform &RenderingPipeline::operator[](const std::string &name){
    programgl.use();
    return programgl[name];
}