//
// created by stiven on 08/11/2019.
//

#include "tilemap.h"
#include <fstream>
#include "json/json.hpp"
#include <utils/stringUtil.h>

namespace fs = std::filesystem;



render::tile_map::tile_map(std::string_view path_to_map) {
    using json = nlohmann::json;
    std::ifstream tile_map_file{path_to_map};
    json j_file;
    tile_map_file >> j_file;

    tile_param tile_map_param;
    tile_map_param.nb_tile_height = j_file["height"];
    tile_map_param.nb_tile_width = j_file["width"];
    tile_map_param.tile_height = j_file["tileheight"];
    tile_map_param.tile_width = j_file["tilewidth"];
    std::string path = utils::extractPath(std::string(path_to_map));

    path += fs::path{j_file["tilesets"][0]["image"]};
    texture = glish3::Texture2D{glish3::Texture2D::readImage(path.c_str())};

    pos position;
    position.x = j_file["layers"][0]["x"];//TODO: multiple layers
    position.y = j_file["layers"][0]["y"];


    sprite_sheet = sprite_sheet_data(j_file["tilesets"][0]);
    auto&[nb_tile_height, nb_tile_width, _, __] = tile_map_param;
    sprites.reserve(nb_tile_height * nb_tile_width * j_file["layers"].size());
    for(auto layers : j_file["layers"]){
        auto layers_data = layers["data"];
        handle_layer(layers_data, position, tile_map_param);
    }

    sprites.shrink_to_fit();

}

void render::tile_map::handle_layer(nlohmann::json const &layer, const render::tile_map::pos &position,
                                    const render::tile_map::tile_param &param) {
    auto create_tab = [](int nb){
        std::vector<int> vector(nb);
        int i = 0;
        std::generate(std::begin(vector), std::end(vector), [&i]{
            return i++;
        } );
        return vector;
    };

    auto&[nb_tile_height, nb_tile_width, tile_height, tile_width] = param;
    for (auto line : create_tab(param.nb_tile_width)){
        for (auto column : create_tab(param.nb_tile_height)){
            int sprite_number = static_cast< int>(layer.at(line + column*param.nb_tile_width)) - 1;
            if(sprite_number == -1)
                continue;
            sprites.emplace_back(render_data::sprite{
                    .data_texture = {
                            .data_transform = {
                                    .x = position.x + line * tile_width,
                                    .y = position.y + column * tile_height,
                                    .width = tile_width,
                                    .height = tile_height
                            },
                            .texture = texture.get_view()
                    },
                    .sprite_number = sprite_number,
                    .sprite_sheet_datas = &sprite_sheet
            });
        }
    }
}



