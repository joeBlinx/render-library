//
// Created by stiven on 05/11/2019.
//

#ifndef GRAPHICS_RENDERING_TEXTURE2D_VIEW_H
#define GRAPHICS_RENDERING_TEXTURE2D_VIEW_H
#include <glish3/gl_glew.hpp>


namespace glish3 {
class Texture2D;
namespace view {
struct texture2d_view {
    texture2d_view(Texture2D const &texture2d);

    void bind() const;

    void activeTexture(int id) const;

private:
    GLuint textureId = 0;
};
}
}
#endif //GRAPHICS_RENDERING_TEXTURE2D_VIEW_H
