#ifndef AUTO_GENERATE_GLISH3_HEADER
#define AUTO_GENERATE_GLISH3_HEADER

#include <glish3/glfunction.hpp>
#include <glish3/gl_glew.hpp>
#include <glish3/log/errorHandler.hpp>
#include <glish3/log/log.hpp>
#include <glish3/programGL.hpp>
#include <glish3/shader.hpp>
#include <glish3/texture2d.hpp>
#include <glish3/uniform.hpp>
#include <glish3/Vao.hpp>
#include <glish3/Vbo.hpp>

#endif //AUTO_GENERATE_GLISH3_HEADER