//
// Created by joe on 2/13/19.
//

#ifndef TESTGLISH3_TEXTURE_HPP
#define TESTGLISH3_TEXTURE_HPP

#include <glish3/gl_glew.hpp>
#include <string>
#include <memory>
#include <SDL2/SDL_surface.h>
#include <glish3/view/texture2d_view.h>

namespace glish3 {
    struct image{
        int chan;
        void* data;
    };
	struct deleter
	{
		void operator()(image * surface);
	};
	struct texture_settings
	{
		int width;
		int height;
		void * data;
		std::unique_ptr<image, deleter> surface;

	};
	class Texture2D {

		GLuint textureId = 0;
		static GLenum constexpr target = GL_TEXTURE_2D;
        friend struct glish3::view::texture2d_view;
	public:
		Texture2D() = default;
		Texture2D(const texture_settings &settings);
		Texture2D(std::string_view path_image);
		void bind()const;

		void activeTexture(int id)const;

		Texture2D(Texture2D const &) = delete;

		Texture2D &operator=(Texture2D const &) = delete;

		Texture2D(Texture2D  && rhs) ;

		Texture2D &operator=(Texture2D && rhs);

		static texture_settings readImage(std::string_view path);

		operator view::texture2d_view()const;
        view::texture2d_view get_view() const;

		~Texture2D();
	};
}


#endif //TESTGLISH3_TEXTURE_HPP
