//
// Created by joe on 23/09/18.
//

#ifndef GLISH3_LOG_HPP
#define GLISH3_LOG_HPP

#include <fstream>
#include <string_view>
struct Log{

	Log() = default;
	Log(const std::string &path, std::string const &project = "undefined");

	void info(std::string_view info);

	void warning(std::string_view warn);

	void error(std::string_view error);

	void title(std::string_view title);

	void fileNotFound (std::string_view file);

	Log(Log&&) = default;
	Log&operator=(Log&&) = default;
	~Log();

	bool isInit() const;
private:
	std::ofstream _stream;
	std::string _time;
	std::string _project;

	bool _init = false;
	void initHTML();
	void initCSS();
};
#endif //GLISH3_LOG_HPP
