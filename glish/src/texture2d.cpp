//
// Created by joe on 2/13/19.
//

#include <glish3/texture2d.hpp>

#include "glish3/texture2d.hpp"

#include <glish3/glfunction.hpp>
#include <SDL2/SDL_image.h>
#include "log/errorHandler.hpp"
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"
namespace glish3 {
	Texture2D::Texture2D(std::string_view path_image):Texture2D(readImage(path_image)){

	}
	Texture2D::Texture2D(const texture_settings &settings) {

		glishGenTextures(1, &textureId);
		bind();
        glishTexImage2D(target, 0, GL_RGBA, settings.width,
                        settings.height,
                        0, GL_RGBA, GL_UNSIGNED_BYTE,
                        settings.data);


		glTexParameteri(target, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

		glTexParameteri(target, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

	}

	void Texture2D::bind()const {
		glishBindTextures(target, textureId);
	}

	void Texture2D::activeTexture(int id) const{

		glishActiveTexture(GL_TEXTURE0 + id);
		bind();
	}

	Texture2D::~Texture2D() {
		if(textureId) {
			glishDeleteTextures(1, &textureId);
			textureId = 0;
		}

	}

	Texture2D::Texture2D(Texture2D &&rhs) :textureId(rhs.textureId){
		rhs.textureId = 0;
	}

	Texture2D &Texture2D::operator=(Texture2D &&rhs) {
		textureId = rhs.textureId;
		rhs.textureId = 0;

		return *this;
	}

	texture_settings Texture2D::readImage(std::string_view path) {
	    int w, h, channel;
        stbi_set_flip_vertically_on_load(false);
        auto* image = stbi_load(path.data(), &w, &h, &channel, STBI_rgb_alpha);
        if(!image){
            log.fileNotFound(path);
        }
        decltype(texture_settings::surface) a (new glish3::image{channel, image});
		texture_settings textureSettings{w, h, image, std::move(a) };
		return textureSettings;
	}

Texture2D::operator view::texture2d_view() const {
    return view::texture2d_view(*this);
}

view::texture2d_view Texture2D::get_view() const {
    return static_cast<view::texture2d_view>(*this);
}

void deleter::operator()(image* surface) {
    stbi_image_free(surface->data);
}
}
