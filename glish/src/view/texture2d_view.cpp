//
// Created by stiven on 05/11/2019.
//
#include <glish3/glfunction.hpp>
#include "view/texture2d_view.h"
#include "texture2d.hpp"
namespace glish3::view{


void texture2d_view::activeTexture(int id) const {
    glishActiveTexture(GL_TEXTURE0 + id);
    bind();
}

texture2d_view::texture2d_view(glish3::Texture2D const &texture2d): textureId(texture2d.textureId) {

}

void texture2d_view::bind()const {
    glishBindTextures(glish3::Texture2D::target, textureId);
}

}